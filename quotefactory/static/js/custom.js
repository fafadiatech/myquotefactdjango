csrfval = $('input[name="csrfmiddlewaretoken"]').val();
$('#previous_b').addClass('hidden');
$('#previous_a').addClass('hidden');
// for active class
$(".nav a").on("click", function(){ 
    $(".nav").find(".active").removeClass("active"); 
    $(this).parent().addClass("active"); 
});
// ends

//for checkbox
$(function () {
    $('select').multipleSelect();
});
//ends

//for arrow image 1,2 and default
function clickNext(questionClassId,nextButton,previousButton) {
    var current = $("."+questionClassId+":visible"),
    last = $("."+questionClassId+":last");
    $(current).next().show();
    $(current).hide();
    if($('#'+previousButton).is(':hidden')){
        $('#'+previousButton).removeClass('hidden');
    }
    if ($(current).next().is(':last-child')) {
        $('.submit').removeClass('hidden');
        $('.cancel').addClass('hidden');
        $('#'+nextButton).addClass('hidden');
    }
}

//function to display previous question
function clickPrevious(questionClassId,previousButton,nextButton) {

    var current = $("."+questionClassId+":visible")
    first = $("."+questionClassId+":first-child");
    if ($(current).is(':first-child')) {
        $('#'+previousButton).addClass('hidden');
    }
    else {
        if($('#'+nextButton).is(':hidden')){
            $('#'+nextButton).removeClass('hidden');
        }
        $(current).prev().show();
        $(current).hide();
        if($('#'+previousButton).is(':hidden')){
            $('#'+previousButton).removeClass('hidden');
        } 
    }
}

//toggle on image talent spare & consume
function onClickImageToggleForSectionA(thisSection,nextButton,ifOtherSection1,formIdd,questionClassIdd) {
    $("#"+formIdd).trigger('reset');
    $("#industry-sector-form").trigger('reset');
    location.reload;
    $('.block_a_error').html('');
    if($('#previous_a').is(':visible')) {
        $('#previous_a').addClass('hidden');
    }

    $('#'+thisSection).toggle();

    current = $("."+questionClassIdd+":visible");
    $(current).hide();
    $("."+questionClassIdd+":first-child").show();

    if($('.submit').is(':visible')) {
        $('.submit').addClass('hidden');
        $('.cancel').removeClass('hidden');
    }
    if($('#'+nextButton).is(':hidden')) {
        $('#'+nextButton).removeClass('hidden');
        }
    if($('#'+ifOtherSection1).is(':visible')) {
        $('#'+ifOtherSection1).toggle();
    }
}
$('#talent').click(function() {
    onClickImageToggleForSectionA('section2a','next_a','section2b','talent-project-form','aquestion');  
});


// function to cancel button and redirect them to first page
function cancelClick(sectionId,formId) {
    $('#'+sectionId).toggle();
    $("#"+formId).trigger('reset');
    window.location.reload();
    return false;
}
$('#cancela').click(function() {
    cancelClick('section2a','theforma')
});

$('#cancelb').click(function(){
   cancelClick('section2b','theformb') 
});

//for start again//
function startAgainClick(formId1,questionClassId1,arrowImgId1) {
    location.reload;
    $("#"+formId1).trigger('reset');
    $('.specialism_label1').find('span').text('');
    $('.specialism_label').find('span').text('');
    $('#indus').remove();
    current = $("."+questionClassId1+":visible");
    $(current).hide();
    $("."+questionClassId1+":first-child").show();
     if($('.submit').is(':visible')) {
        $('.cancel').removeClass('hidden');
        $('#'+arrowImgId1).removeClass('hidden');
        $('.submit').addClass('hidden');
    }
    if($('#'+arrowImgId1).is(':hidden')) {
        $('#'+arrowImgId1).removeClass('hidden');
    }
}
$('#startagaina').click(function() {
    startAgainClick('talent-project-form','aquestion','next_a')
});
$('#startagainb').click(function() {
    startAgainClick('industry-sector-form','bquestion','next_b')
});
// start again ends//

$('#submit_btn').on('click', function () {
    $('#myModal').modal('hide');
    $('#myModal1').modal('show');
});

$("#pay_now").click(function(e) {
    var valid = $('#payment-form').parsley().validate();
    if( valid == true){
        csrfval = $('input[name="csrfmiddlewaretoken"]').val();
        var card_name = $("#card_name").val();
        var amount = $("#payment").val();
        var cardNumber = $("#holder_number").val();
        var email = $("#payment_email").val();
        var exp_month = $("#exp_month").val();
        var exp_year = $("#exp_year").val();
        var cvc = $("#cvc").val();
        var currency = $("#amount_in").val();

        $.ajax({
        url: "/frontend/payment-sucessfull/",
        type: "POST",
        data: {"card_name":card_name, "currency":currency, "amount":amount, "cardNumber":cardNumber, "exp_month":exp_month, "exp_year":exp_year, "cvc":cvc, "email":email,csrfmiddlewaretoken:csrfval},
        beforeSend:function() {
            $('.spinner1').removeClass('hidden');
            },
        success : function() {
            $('.spinner1').addClass('hidden');
            $('.sucessmsg').html("<h4>Payment done sucessfully, Please check your Mail.</h4>");
            window.setTimeout(function(){location.reload()},9000)
            },
        error: function() {
            $('.spinner1').addClass('hidden');
            $('.sucessmsg').html("<h4>Something went wrong,Please try again.</h4>");
            }
        });
    return false;
    }
    else {
        e.preventDefault();
    }   
});

$('#add-talent-project').click(function(){
    var formData = new FormData($("#talent-project-form")[0]);
    $.ajax({
        url: "/frontend/talent-project-add/",
        type: "POST",
        headers: { 'X-CSRFToken': csrfval },
        data:formData,
        processData: false,
        contentType: false,
         beforeSend:function(){
            $.blockUI({ message: '<h4>Please wait...</h4><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></h3>',
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    opacity: .5,
                    color: '#fff',
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    left:'43%',
                    width:'auto',

                }
             });
            },
        success : function(data) {
            $.unblockUI();
            $('#myModal').modal('show');
            }
        });
    
    })

$('#add-industry-sector').click(function(){
    var formData = new FormData($("#industry-sector-form")[0]);
    $.ajax({
        url: "/frontend/equipment-consumables-add/",
        type: "POST",
        headers: { 'X-CSRFToken': csrfval },
        data:formData,
        processData: false,
        contentType: false,
        beforeSend:function(){
            $.blockUI({ message: '<h4>Please wait...</h4><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></h3>',
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    opacity: .5,
                    color: '#fff',
                    '-webkit-border-radius': '10px', 
                    '-moz-border-radius': '10px', 
                    left:'43%',
                    width:'auto',

                }
             });
            },
        success : function(data) {
            $.unblockUI();
            $('#myModal').modal('show');
            }
    })
});

$(function () {
  var $sections = $('.form-section');

    function navigateTo(index) {
    // Mark the current section with the class 'current'
        $sections
        .removeClass('current')
        .eq(index)
        .addClass('current');
        // Show only the navigation buttons that make sense for the current section:
        $('.form-navigation #previous_a').toggle(index > 0);
        var atTheEnd = index >= $sections.length - 1;
        $('.form-navigation #next_a').toggle(!atTheEnd);
        $('.form-navigation [type=submit]').toggle(atTheEnd);
    }

    function curIndex() {
        // Return the current index by looking at which section has the class 'current'
        return $sections.index($sections.filter('.current'));
    }
    $('#startagaina').click(function() {
        $('.block_a_error').html('');
        navigateTo(0)
    });

    $('.form-navigation #previous_a').click(function() {
        navigateTo(curIndex() - 1);
        clickPrevious('aquestion','previous_a','next_a');
        $('.block_a_error').html('');
    });

    $('.form-navigation .nexta').click(function() {
        if ($('#talent-project-form').parsley().validate({group: 'block-' + curIndex()}))
        {
            $('.block_a_error').html('');
            var check = $('#talent-project-form').parsley().isValid({group: 'block-' + curIndex()})
            navigateTo(curIndex() + 1);
            clickNext('aquestion','next_a','previous_a');
        }
        if (check == null) {
            $('.block_a_error').html('Please Fill Question '+ (curIndex()+1));
        }
    });
    $sections.each(function(index, section) {
        $(section).find(':input').attr('data-parsley-group', 'block-' + index);
    });
    navigateTo(0);
});

// For other section
$(function () {
    var $sectionsB = $('.form-section1');

    function navigateToB(index) {
    // Mark the current section with the class 'current'
        $sectionsB
        .removeClass('current')
        .eq(index)
        .addClass('current');
        // Show only the navigation buttons that make sense for the current section:
        $('.form-navigation1 #previous_b').toggle(index > 0);
        var atTheEnd = index >= $sectionsB.length - 1;
        $('.form-navigation1 #next_b').toggle(!atTheEnd);
        $('.form-navigation1 [type=submit]').toggle(atTheEnd);
    }

    function curIndexB() {
        // Return the current index by looking at which section has the class 'current'
        return $sectionsB.index($sectionsB.filter('.current'));
    }
    $('#startagainb').click(function() {
        $('.block_a_error').html('');
        navigateToB(0)
    });

    $('.form-navigation1 #previous_b').click(function() {
        navigateToB(curIndexB() - 1);
        clickPrevious('bquestion','previous_b','next_b');
        $('.block_a_error').html('');
    });

    $('.form-navigation1 .nextb').click(function() {
    if ($('#industry-sector-form').parsley().validate({group: 'block-' + curIndexB()}))
        {
            $('.block_a_error').html('');
            var check = $('#industry-sector-form').parsley().isValid({group: 'block-' + curIndexB()})
            navigateToB(curIndexB() + 1);
            clickNext('bquestion','next_b','previous_b');
        }
        if (check == null) {
            $('.block_a_error').html('Please Fill Question '+ (curIndexB()+1));
        }
    });
    $sectionsB.each(function(index, sectionB) {
        $(sectionB).find(':input').attr('data-parsley-group', 'block-' + index);
    });
    function onClickToggleSectionBandC(thisClass,otherClass) { 
        $("#industry-sector-form").trigger('reset');
        $("#talent-project-form").trigger('reset');
        location.reload;
        $('.block_a_error').html('');
        navigateToB(0); 

        if($("#previous_b").is(':visible')){
            $('#previous_b').addClass('hidden')
        }
        if($('#section2a').is(':visible')) {
            $('#section2a').hide();
            $('.submit').addClass('hidden');
            $('.cancel').removeClass('hidden');
        }
        if($('.submit').is(':visible')) {
            $('.submit').addClass('hidden');
            $('.cancel').removeClass('hidden');
        }
        $("#theformb").trigger('reset');

        $('#section2b').addClass(thisClass);

        if($('#section2b').hasClass(otherClass)) {
            $('#section2b').removeClass(otherClass)
            current = $(".bquestion:visible");
            $(current).hide();
            $(".bquestion:first-child").show();
        } else {
            $('#section2b').toggle();
            current = $(".bquestion:visible");
            $(current).hide();
            $(".bquestion:first-child").show();
        }
        if($('#next_b').is(':hidden')) {
            $('#next_b').removeClass('hidden');
        }
    }
    $('#spare').click(function() {
        onClickToggleSectionBandC('inSpares','inConsume');
    });
    $('#consume').click(function() {
        onClickToggleSectionBandC('inConsume','inSpares');
    }); 

    navigateToB(0);
});

var the_terms = $("#check");

the_terms.click(function() {
    if ($(this).is(":checked")) {
        $("#submit_btn").removeAttr("disabled");
    } else {
        $("#submit_btn").attr("disabled", "disabled");
    }
});

$('#close_modal').click(function(){
    window.location.reload();
})
$('#close_modal1').click(function(){
    $('.payment-form').trigger('reset');
    window.location.reload();
})

$('#myModal1').on('show.bs.modal', function () {
    $(".ms-parent").remove();
    $("#payment-form").find("select").each(function(){
        $(this).css('display','block');
    });
    // $("#abc").show();
});