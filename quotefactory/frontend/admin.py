from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Specialism)
admin.site.register(IndustrySector)
admin.site.register(TalentService)
admin.site.register(EquipmentConsumablesService)