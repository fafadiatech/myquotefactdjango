from __future__ import unicode_literals

from django.db import models

# Create your models here.
class Specialism(models.Model):
	"""docstring for Specialism"""
	name = models.CharField(max_length=255)

	def __unicode__(self):
		return self.name


class IndustrySector(models.Model):
	"""docstring for IndustrySector"""
	name = models.CharField(max_length=255)

	def __unicode__(self):
		return "%s" % (self.name)
		
class TalentService(models.Model):
	"""docstring for TalentService"""
	term = models.CharField(max_length=255)
	name = models.CharField(max_length=255)
	email = models.CharField(max_length=255)
	by_when = models.CharField(max_length=255)
	location = models.CharField(max_length=255)
	telephone = models.CharField(max_length=255)
	designation = models.CharField(max_length=255)
	postal_code = models.CharField(max_length=255)
	specialism = models.ManyToManyField(Specialism)
	contract = models.CharField(max_length=255, blank=True, null=True)
	education = models.CharField(max_length=255, blank=True, null=True)
	expectation = models.TextField(max_length=255,blank=True, null=True)
	working_pattern = models.CharField(max_length=255, blank=True, null=True)
	level_of_expertise = models.CharField(max_length=255, blank=True, null=True)
	file_upload = models.FileField(upload_to='uploadedfile', blank=True, null=True)

	def __unicode__(self):
		return self.term

class EquipmentConsumablesService(models.Model):
	"""docstring for ClassName"""
	name = models.CharField(max_length=255, blank=True, null=True)
	email = models.CharField(max_length=255, blank=True, null=True)
	by_when = models.CharField(max_length=255, blank=True, null=True)
	location = models.CharField(max_length=255, blank=True, null=True)
	quantity = models.TextField(max_length=255, blank=True, null=True)
	telephone = models.CharField(max_length=255, blank=True, null=True)
	description = models.CharField(max_length=255, blank=True, null=True)
	designation = models.CharField(max_length=255, blank=True, null=True)
	postal_code = models.CharField(max_length=255, blank=True, null=True)
	industry_sector = models.ManyToManyField(IndustrySector, blank=True, null=True)
	specification = models.TextField(blank=True, null=True)
	product = models.CharField(max_length=255, blank=True, null=True)
	order_cycle = models.CharField(max_length=255, blank=True, null=True)
	file_upload = models.FileField(upload_to='uploadedfile', blank=True, null=True)

	def __unicode__(self):
		return  self.quantity
