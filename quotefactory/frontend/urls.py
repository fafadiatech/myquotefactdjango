from django.conf.urls import patterns, include, url
from frontend.views import *
from frontend import views

urlpatterns = patterns('',
	url(
		regex=r'^talent-project-add/$',
        view=TalentProjectAddView.as_view(),
        name='talent-project-add'
		),
	url(
		regex=r'^equipment-consumables-add/$',
        view=EquipmentConsumablesAddView.as_view(),
        name='equipment-consumables-add'
		),
	url(
		regex=r'^payment-sucessfull/$',
        view=PayNowView.as_view(),
        name='payment-sucessfull'
		),
)