from django import forms
from django.contrib import auth
from frontend.models import *

class TalentProjectAddForm(forms.ModelForm):
	"""
	Talent Project Add Form
	"""
	class Meta:
		model = TalentService
		exclude = []


class EquipmentConsumablesAddForm(forms.ModelForm):
	"""
	Talent Project Add Form
	"""
	class Meta:
		model = EquipmentConsumablesService
		exclude = []
