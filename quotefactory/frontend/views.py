import stripe
import json
from datetime import datetime
from django.shortcuts import render
from frontend.models import *
from frontend.forms import *
from django.core.urlresolvers import reverse, reverse_lazy
from django.http import HttpResponseRedirect
from reportlab.pdfgen import canvas
from django.http import HttpResponse
from reportlab.lib.pagesizes import letter
from django.core.mail import send_mail
from django.core.mail import EmailMessage
from django.core.mail import EmailMultiAlternatives
from django.views.generic import TemplateView, FormView, View
from django.core.mail.message import EmailMultiAlternatives
# Create your views here.


class HomeView(TemplateView):
	template_name = 'frontend/index.html'

	def get_context_data(self, **kwargs):
		context = super(HomeView, self).get_context_data(**kwargs)
		context['days'] = [day for day in range(1, 32)]
		context['months'] = [(month, datetime(2016, month, 1).strftime('%B')) for month in range(1, 13)]
		years = [year for year in range(2016, datetime.now().year+30)]
		years.reverse()
		context['years'] = years
		context['specialism'] = Specialism.objects.all()
		context['industry_sector'] = IndustrySector.objects.all()
		return context


class TalentProjectAddView(FormView):
	template_name = 'frontend/index.html'
	form_class = TalentProjectAddForm
	success_url = reverse_lazy('home')

	def get_context_data(self, **kwargs):
		context = super(TalentProjectAddView, self).get_context_data(**kwargs)
		return context

	def form_valid(self, form):
		if form.is_valid():
			talent_obj = form.save()
			specialism_list = [Specialism.objects.get(id=i) for i in self.request.POST.getlist('specialism')]
			for speci_obj in specialism_list:
				talent_obj.specialism.add(speci_obj)
				talent_obj.save()
			if self.request.POST['email']:
				user_email = self.request.POST['email']	
			form.save()
			r = send(talent_obj, user_email)
			error_msg = "form submitted successfully"
			return self.render_to_response(self.get_context_data(form=form))

	def form_invalid(self, form):
		print form.errors
		error_msg = "There was an error in your registration form!"
		return self.render_to_response(self.get_context_data(form=form))


class EquipmentConsumablesAddView(FormView):
	template_name = 'frontend/index.html'
	form_class = EquipmentConsumablesAddForm
	success_url = reverse_lazy('home')

	def get_context_data(self, **kwargs):
		context = super(EquipmentConsumablesAddView, self).get_context_data(**kwargs)
		return context

	def form_valid(self, form):
		if form.is_valid():
			equipment_consumables_obj = form.save()
			industry_list = [IndustrySector.objects.get(id=i) for i in self.request.POST.getlist('industry_sector')]
			for equipment_c_obj in industry_list:
				equipment_consumables_obj.industry_sector.add(equipment_c_obj)
				equipment_consumables_obj.save()
			form.save()
			if self.request.POST['email']:
				user_email = self.request.POST['email']
				r = send(equipment_consumables_obj, user_email)
			error_msg = "form submitted successfully"
			return self.render_to_response(self.get_context_data(form=form))

	def form_invalid(self, form):
		print form.errors
		error_msg = "There was an error in your registration form!"
		return self.render_to_response(self.get_context_data(form=form))


def generate_pdf_(obj):
	response = HttpResponse(content_type='application/pdf')
	response['Content-Disposition'] = 'attachment; filename="data.pdf"'
	c = canvas.Canvas(response, pagesize=letter)
	width, height = letter
	count = 0
	if type(obj) == type(EquipmentConsumablesService()):
		c.setFont('Helvetica-Bold',10)
		c.drawString(60, height/2+320-count, 'Industry Sector : ')
		c.setFont('Helvetica',10)
		for i in obj.industry_sector.all():
			c.drawString(300,  height/2+320-count, str(i.name))
			count += 20

		c.setFont('Helvetica-Bold',10)
		c.drawString(60, height/2+320-count, 'Description of what you are looking for?: ')
		c.setFont('Helvetica',10)
		c.drawString(300,  height/2+320-count, str(obj.description))
		count += 20

		c.setFont('Helvetica-Bold',10)
		c.drawString(60, height/2+320-count, 'Quantity : ')
		c.setFont('Helvetica',10)
		c.drawString(300,  height/2+320-count, str(obj.quantity))
		count += 20

		c.setFont('Helvetica-Bold',10)
		c.drawString(60, height/2+320-count, 'Order Cycle : ')
		c.setFont('Helvetica',10)
		c.drawString(300,  height/2+320-count, str(obj.order_cycle))
		count += 20

		c.setFont('Helvetica-Bold',10)
		c.drawString(60, height/2+320-count, 'Specifications : ')
		c.setFont('Helvetica',10)
		c.drawString(300,  height/2+320-count, str(obj.specification))
		count += 20

	if type(obj) == type(TalentService()):
		c.setFont('Helvetica-Bold',10)
		c.drawString(60, height/2+320-count, 'Term : ')
		c.setFont('Helvetica',10)
		c.drawString(300,  height/2+320-count, str(obj.term))
		count += 20

		c.setFont('Helvetica-Bold',10)
		c.drawString(60, height/2+320-count, 'Contract : ')
		c.setFont('Helvetica',10)
		c.drawString(300,  height/2+320-count, str(obj.contract))
		count += 20

		c.setFont('Helvetica-Bold',10)
		c.drawString(60, height/2+320-count, 'Specialism : ')
		c.setFont('Helvetica',10)
		for j in obj.specialism.all():
			c.drawString(300,  height/2+320-count, str(j.name))
			count += 20

		c.setFont('Helvetica-Bold',10)
		c.drawString(60, height/2+320-count, 'Education : ')
		c.setFont('Helvetica',10)
		c.drawString(300,  height/2+320-count, str(obj.education))
		count += 20

		c.setFont('Helvetica-Bold',10)
		c.drawString(60, height/2+320-count, 'Level of Expertise : ')
		c.setFont('Helvetica',10)
		c.drawString(300,  height/2+320-count, str(obj.level_of_expertise))
		count += 20

		c.setFont('Helvetica-Bold',10)
		c.drawString(60, height/2+320-count, 'Working Pattern : ')
		c.setFont('Helvetica',10)
		c.drawString(300,  height/2+320-count, str(obj.working_pattern))
		count += 20

		c.setFont('Helvetica-Bold',10)
		c.drawString(60, height/2+320-count, 'Any Expectations?? : ')
		c.setFont('Helvetica',10)
		c.drawString(300,  height/2+320-count, str(obj.expectation))
		count += 20


	c.setFont('Helvetica-Bold',10)
	c.drawString(60, height/2+320-count, 'Location : ')
	c.setFont('Helvetica',10)
	c.drawString(300,  height/2+320-count, str(obj.location))
	count += 20

	c.setFont('Helvetica-Bold',10)
	c.drawString(60, height/2+320-count, 'By When?? : ')
	c.setFont('Helvetica',10)
	c.drawString(300,  height/2+320-count, str(obj.by_when))
	count += 20
	
	if count > 500:
		c.showPage()
		count = 0

	c.setFont('Helvetica-Bold',12)
	c.drawString(60, height/2+320-count, 'Contact Details ')
	count += 20
	
	c.setFont('Helvetica-Bold',10)
	c.drawString(60, height/2+320-count, 'Name : ')
	c.setFont('Helvetica',10)
	c.drawString(300,  height/2+320-count, str(obj.name))
	count += 20

	c.setFont('Helvetica-Bold',10)
	c.drawString(60, height/2+320-count, 'Designation : ')
	c.setFont('Helvetica',10)
	c.drawString(300,  height/2+320-count, str(obj.designation))
	count += 20

	c.setFont('Helvetica-Bold',10)
	c.drawString(60, height/2+320-count, 'Email : ')
	c.setFont('Helvetica',10)
	c.drawString(300,  height/2+320-count, str(obj.email))
	count += 20

	c.setFont('Helvetica-Bold',10)
	c.drawString(60, height/2+320-count, 'Telephone : ')
	c.setFont('Helvetica',10)
	c.drawString(300,  height/2+320-count, str(obj.telephone))
	count += 20

	c.setFont('Helvetica-Bold',10)
	c.drawString(60, height/2+320-count, 'Postal Code : ')
	c.setFont('Helvetica',10)
	c.drawString(300,  height/2+320-count, str(obj.postal_code))
	count += 20

	c.showPage()
	c.save()
	pdf = response.getvalue()
	response.close()
	return pdf


def send(obj, user_email):
	pdf = generate_pdf_(obj)
	msg = EmailMultiAlternatives('Enquiry for Quote', 'You have received a Quote for the following:',"chintan.desai@myquotefactory.com",['chintan.desai@myquotefactory.com',user_email])
	msg.attach('my_pdf.pdf', pdf, 'application/pdf')
	if obj.file_upload:
		type1 = obj.file_upload.url
		type1 = type1.split('.')[-1]
		msg.attach('uploaded_file.'+type1, obj.file_upload.read(), 'application/'+type1)
	msg.content_subtype = "html"
	msg.send()


class PayNowView(FormView):
	"""
	Paynow E-mail notification
	"""
	template_name = 'frontend/index.html'
	success_url = '/'

	def render_to_json_response(self, context, **response_kwargs):
		data = json.dumps(context)
		response_kwargs['content_type'] = 'application/json'
		response = HttpResponse(data, **response_kwargs)
		return response

	def post(self, request, *args, **kwargs):
		currency = request.POST.get('currency')
		card_name = request.POST.get('card_name')
		email = request.POST.get('email')
		amount = request.POST.get('amount')
		cardNumber = request.POST.get('cardNumber')
		exp_month = request.POST.get('exp_month')
		exp_year = request.POST.get('exp_year')
		cvc = request.POST.get('cvc')
		stripe.api_key = "sk_test_6OPddZJp0CXRJMMFqvuNSkjW"
		token = stripe.Token.create(
		    card={
		    	"name": card_name,
		        "number": cardNumber,
		        "exp_month": exp_month,
		        "exp_year": exp_year,
		        "cvc": cvc
		    },
		)
		
		customer = stripe.Customer.create(source=token, description="customer")
		if currency == "Dollar":
			stripe.Charge.create(amount=amount, currency="usd", customer=customer.id)
		if currency == "Pound":
			stripe.Charge.create(amount=amount, currency="gbp", customer=customer.id)
		msg = "You've successfully paid, Check your email for details."
		send_mail('MuQuoteFactory Payment Details','Hi, '+card_name+' Your payment is successful '+'Amount: '+str(amount), 'chintan.desai@myquotefactory.com',[email, 'chintan.desai@myquotefactory.com'], fail_silently=False)
		return self.render_to_json_response({"msg":msg})
