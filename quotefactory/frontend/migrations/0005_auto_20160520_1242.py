# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0004_auto_20160520_0754'),
    ]

    operations = [
        migrations.DeleteModel(
            name='CustomerCard',
        ),
        migrations.AlterField(
            model_name='equipmentconsumablesservice',
            name='by_when',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='equipmentconsumablesservice',
            name='description',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='equipmentconsumablesservice',
            name='designation',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='equipmentconsumablesservice',
            name='email',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='equipmentconsumablesservice',
            name='industry_sector',
            field=models.ManyToManyField(to='frontend.IndustrySector'),
        ),
        migrations.AlterField(
            model_name='equipmentconsumablesservice',
            name='location',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='equipmentconsumablesservice',
            name='name',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='equipmentconsumablesservice',
            name='postal_code',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='equipmentconsumablesservice',
            name='quantity',
            field=models.TextField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='equipmentconsumablesservice',
            name='telephone',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
    ]
