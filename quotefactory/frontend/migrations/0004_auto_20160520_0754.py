# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('frontend', '0003_auto_20160401_0730'),
    ]

    operations = [
        migrations.CreateModel(
            name='CustomerCard',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('stripe_card', models.CharField(max_length=255)),
                ('card_number', models.CharField(max_length=255)),
                ('email', models.EmailField(max_length=254)),
                ('expires', models.CharField(max_length=255)),
                ('ccv', models.CharField(max_length=255)),
            ],
        ),
        migrations.AlterField(
            model_name='equipmentconsumablesservice',
            name='file_upload',
            field=models.FileField(null=True, upload_to='uploadedfile', blank=True),
        ),
        migrations.AlterField(
            model_name='equipmentconsumablesservice',
            name='industry_sector',
            field=models.ManyToManyField(to='frontend.IndustrySector', null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='equipmentconsumablesservice',
            name='order_cycle',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='equipmentconsumablesservice',
            name='product',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='equipmentconsumablesservice',
            name='quantity',
            field=models.TextField(max_length=255),
        ),
        migrations.AlterField(
            model_name='equipmentconsumablesservice',
            name='specification',
            field=models.TextField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='talentservice',
            name='contract',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='talentservice',
            name='education',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='talentservice',
            name='expectation',
            field=models.TextField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='talentservice',
            name='file_upload',
            field=models.FileField(null=True, upload_to='uploadedfile', blank=True),
        ),
        migrations.AlterField(
            model_name='talentservice',
            name='level_of_expertise',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='talentservice',
            name='working_pattern',
            field=models.CharField(max_length=255, null=True, blank=True),
        ),
    ]
